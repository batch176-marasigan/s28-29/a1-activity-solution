
const express = require("express");

const port =4000;

const application = express();

application.use(express.json());

application.listen(port,()=> console.log(`Express API Server running on port : ${port}`));
 
application.get('/',(request,response) => {
     
    
    response.send('Welcome to my To do list');


});


const tasks=[]; 

//----------- [POST - CREATE TASK] ----------TASK1 
application.post('/task',(req,res) =>{    
    //console.log(req.body);  //SHOW IN GITBASH  TERMINAL 
    let newTask = req.body;
    let taskName = req.body.name;
    let taskStatus = req.body.status;
    let message; 
    
    if(taskName!=='' && taskStatus !==''  ){ 
        let existingTask=0 //set the indicator to false  
        for (let indexNum = 0; indexNum < tasks.length; indexNum++) {
                if (taskName === tasks[indexNum].name){
                    existingTask=1; 
                    break;
                }//console.log(existingTask)
        }
        
        if(existingTask==0 ){             
            tasks.push(newTask);
            message = `New Task "${taskName}" has been added in the Task List.`;
                }
        else {
                 message = `Duplicate task name: '${taskName}' found!. Task creation has been rejected!`;
                }
  }else
    {message = 'No new task added! Please make sure to provide data for task Name and Status'
    }
res.send(message);
      
});


//------[GET] RETRIEVE-----------TASK 2
application.get('/tasks',(req,res) =>
    {    res.send(tasks)
    });


//-------------[DELETE TASK]--------TASK 3
application.delete('/task',(req, res) => 
{
    let message;
    let delTask = req.body.name;
    if (tasks.length !== 0) {
        for (let index = 0; index < tasks.length; index++ ) {
            if (delTask === tasks [index].name) {
                tasks.splice(index,1);        
                message = `${delTask} has been found and deleted from the to do list`;
                break;
           } else {
              message  =   `search name: '${delTask}' not found!`;
             };
        };
   } else 
        {message = 'Empty collection!'}
  res.send(message)
});


// ----------[PUT- UPDATE]---------TASK 4
application.put('/modify-task', (req, res) => 
   {
      let message; 
      let getTaskName = req.body.name;
      let modTask = req.body.status;
      if (tasks.length !== 0)
       {
        
        for (let index = 0;index < tasks.length; index++)
         {
            if (getTaskName === '')
            {
              message = 'Empty Task Name not Allowed! Please provide correct Task Name to continue.';
              break; 
            } 
 
            else if (getTaskName === tasks[index].name)
            {
              tasks[index].status =modTask;
              message = `${getTaskName} Found and Updated to ${modTask}`;
              break; 
            } 

           else {
              message = `Task Name '${getTaskName}' not Found! Please provide correct Task Name to continue.`;
                };
          };
        } 

     else 
        {
        message = 'NO AVAILABLE TASK NAME FOUND!';
        };
      res.send(message);
  });





//----------- [PUT -existing]---------- TASK 5 - TASK NAME EXIST REJECT THE UPDATE + INFORM USER

application.put('/task-update-reject', (req, res) => {
        
        let message
        let getTaskName = req.body.name;
        let newTaskName = req.body.name;

        if (tasks.length !== 0) {
            let existingTask = false;
            for (let i = 0; i < tasks.length; i++) {
                if (getTaskName === tasks[i].name) {
                    existingTask = true
                    message = `Task name: "${getTaskName}" is already on the list! Update Rejected!`;
                    break;
                   }
                }
            }
         else {
            message = 'Collection is empty';
            }

        res.send(message);
    });








//STRETCH GOAL: create an extended feature for a new request for retrieve. which will retrieve all the tasks whose status are 'Pending'


//------[GET] RETRIEVE-----------
//populate existing task
application.get('/tasks',(req,res) =>
    {    res.send(tasks)
    });

//filter pending task 
application.get('/pending-Task', (req, res) => {
        if (tasks.length !== 0) {
            let pending = [];
            for (let i = 0; i < tasks.length; i++) {
                if (tasks[i].status === 'pending'){
                    pending.push(tasks[i]);
                }
            }
       
         res.send(pending);
        } else {
            res.send('task list is empty'); //no record in get collection
        }
    });